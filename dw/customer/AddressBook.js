var AddressBook = function(){};

AddressBook.prototype.getAddress = function(){};
AddressBook.prototype.createAddress = function(){};
AddressBook.prototype.removeAddress = function(){};
AddressBook.prototype.getPreferredAddress = function(){};
AddressBook.prototype.getAddresses = function(){};
AddressBook.prototype.setPreferredAddress = function(){};
AddressBook.prototype.address=null;
AddressBook.prototype.preferredAddress=null;
AddressBook.prototype.addresses=null;

module.exports = AddressBook;