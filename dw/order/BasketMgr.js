var BasketMgr = function(){};

BasketMgr.prototype.createAgentBasket = function(){};
BasketMgr.prototype.createBasketFromOrder = function(){};
BasketMgr.prototype.deleteBasket = function(){};
BasketMgr.prototype.getBasket = function(){};
BasketMgr.prototype.getBaskets = function(){};
BasketMgr.prototype.getCurrentBasket = function(){};
BasketMgr.prototype.getCurrentOrNewBasket = function(){};
BasketMgr.prototype.getStoredBasket = function(){};

module.exports = new BasketMgr();
